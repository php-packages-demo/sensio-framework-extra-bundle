# sensio-framework-extra-bundle

Configure controllers with annotations.  [sensio/framework-extra-bundle](https://packagist.org/packages/sensio/framework-extra-bundle)

# Unofficial documentation
* [*Symfony ParamConverter: the best friend you don’t know yet*
  ](https://medium.com/@ttbertrand/symfony-paramconverter-the-best-friend-you-dont-know-yet-c31ef2251683)
  2022-08 Thomas BERTRAND

## Deconstruction
* [*Another way for a Symfony API to ingest POST requests - No Bundle*
  ](https://dev.to/gmorel/another-way-for-a-symfony-api-to-ingest-post-requests-no-bundle-5h15)
  2020-07 Guillaume MOREL
  * [@ParamConverter
    ](https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html)
